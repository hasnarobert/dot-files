# Colors
export GREP_OPTIONS='--color=auto' GREP_COLOR='1;32'
export CLICOLOR=1
export LSCOLORS=ExGxFxDxCxHxHxCbCeEbEb	

# Set locale. Is needed by python awscli apparently
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Set PS1 variable
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \[\1\]/'
}

PS1='\[\e[0;36m\]\u@\h\[\e[m\] \[\e[0;32m\]\t\[\e[m\] \[\e[0;33m\]\w\[\e[m\]\[\e[0;31m\]$(parse_git_branch)\[\e[m\] $ '

# Setup dev environment variables
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_55.jdk/Contents/Home
export PATH=/usr/local/bin:${PATH}:${JAVA_HOME}/bin:/usr/local/lib/python2.7/site-packages/django/bin

# Reverse search on up/down arrow
bind '"\e[A":history-search-backward' 2>/dev/null
bind '"\e[B":history-search-forward' 2>/dev/null

# Bash completion
if [ -f `brew --prefix`/etc/bash_completion ]; then
	. `brew --prefix`/etc/bash_completion
fi

# Awscli completion
complete -C aws_completer aws

# alias pandora-proxy='PROXY_STATE=`ps aux | grep "ssh root@162.243.45.240 -p 443 -D 12345 -N" | wc -l`; if [ $PROXY_STATE -eq 2 ]; then echo 'ON'; else echo OFF; fi'
alias ll='ls -la'
alias top="htop"
alias brew-u="brew update; brew upgrade"
alias psf="ps aux | grep -i "
